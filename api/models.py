#encoding:utf-8

from exts import db
from datetime import datetime
from werkzeug.security import generate_password_hash,check_password_hash


class Zoos(db.Model):
	__tablename__='zoos'
	id=db.Column(db.Integer,primary_key=True,autoincrement=True)
	name=db.Column(db.String(100),nullable=False)
	place=db.Column(db.String(100),nullable=False)
	create_time=db.Column(db.DateTime,default=datetime.now)

class Animals(db.Model):
	__tablename__='animals'
	id=db.Column(db.Integer,primary_key=True,autoincrement=True)
	name=db.Column(db.String(100),nullable=False)
	info=db.Column(db.String(100),nullable=False)
	parentid=db.Column(db.Integer,nullable=False)
	create_time=db.Column(db.DateTime,default=datetime.now)