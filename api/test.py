#encoding: utf-8

from flask import Flask ,jsonify ,make_response ,abort ,render_template ,request,redirect
from exts import db
from models import Zoos,Animals
from sqlalchemy import or_
import config

app=Flask(__name__)
db.init_app(app)
app.config.from_object(config)

# @app.route('/todo/V1.0/zoos/',methods=['GET'])
# def get_zoos():	
# 	zoos=[dict(id=x.id, name=x.name, place=x.place,cteate_time=x.create_time)
# 		for x in Zoos.query.order_by(Zoos.id).all()]	
# 	return jsonify({'zoos':zoos})

# @app.route('/todo/V1.0/zoos/',methods=['POST'])
# def create_zoo():
# 	name=request.form.get('name')
# 	place=request.form.get('place')	
# 	zoos=Zoos.query.filter(Zoos.name==name , Zoos.place==place).first()
# 	if zoos:
# 		return jsonify({'error': 'Has exist!' })
# 	else:	
# 		zoos=Zoos(name=name,palce=place)
# 		db.session.add(zoos)
# 		db.session.commit()
# 		return jsonify({'zoos':zoos}),201

# @app.route('/todo/V1.0/zoos/<int:id>',methods=['GET'])
# def search_zoo(id):
# 	zoo=[dict(id=x.id, name=x.name, place=x.place,cteate_time=x.create_time) 
# 		for x in Zoos.query.order_by(Zoos.id==id).first()]	
# 	if zoo:
# 		return jsonify({'zoos':zoo[0]})
# 	else :
# 		abort(404)

# @app.route('/todo/V1.0/zoos/<int:id>',methods=['PUT'])
# def update_zoo(id):
# 	zoos=[dict(id=x.id, name=x.name, place=x.place,cteate_time=x.create_time) 
# 		for x in Zoos.query.order_by(Zoos.id==id).first()]	
# 	if not zoos:
# 		abort(404)

# 	else:
# 		zoos.name=request.form.get('name')
# 		zoos.place=request.form.get('place')
# 		db,session.add(zoos)
# 		db.session.commit()
# 		return jsonify({'result': True})

# @app.route('/todo/V1.0/zoos/<int:id>',methods=['DELECT'])
# def delect_zoo(id):
# 	zoo=[dict(id=x.id, name=x.name, place=x.place,cteate_time=x.create_time) 
# 		for x in Zoos.query.order_by(Zoos.id==id).first()]	
# 	if not zoo :
# 		abort(404)
# 	else:
# 		Zoos.remove(zoo[0])
# 		return jsonify({'result': True})


# @app.route('/todo/V1.0/zoos/<int:id>/animals',methods=['GET'])
# def get_zoo(id):
# 	zoos=[dict(id=x.id, name=x.name, place=x.place,cteate_time=x.create_time) 
# 		for x in Zoos.query.order_by(Zoos.id==id).first()]	
# 	if not zoos:
# 		abort(404)
# 	else:
# 		animals=[dict(id=x.id, name=x.name, info=x.info,partentid=x.partentid,cteate_time=x.create_time) 
# 			for x in Animals.query.order_by(Animals.partentid==zoos.id).all()]
# 		if not animals:
# 			abort(404)
# 		else:	
# 			return jsonify({'animals': animals})

@app.route('/todo/V1.0/zoos/<int:id>/animals/<int:animal_id>',methods=['DELECT'])
def delect_zoos(id,animal_id):
	zoos=[dict(id=x.id, name=x.name, place=x.place,cteate_time=x.create_time) 
		for x in Zoos.query.order_by(Zoos.id==id).first()]
	if not zoos:
		abort(404)
	else:
		animals=[dict(id=x.id, name=x.name, info=x.info,partentid=x.partentid,cteate_time=x.create_time) 
			for x in Animals.query.order_by(Animals.partentid==zoos.id , Animals.id==animal_id).first()]
		if not animals:
			abort(404)
		else:	
			return jsonify({'animals': animals})

@app.errorhandler(404)
def not_found(error):
	return make_response(jsonify({'error':'Not found'}),404)





if __name__=='__main__':
	app.run()